import React from 'react';
import styled from 'styled-components'
import background from './utils/images/tris-background.jpeg';

import ButtonsContainer from './components/ButtonsContainer/ButtonsContainer.component';
import Game from './components/Game/Game.component';
import GameResult from './components/GameResult/GameResult.component';

import { useSelector } from 'react-redux';

const App = () => {

  const data = useSelector(state => state.data);
  const { playerVsComputer, playerVsPlayer, isGameOver } = data;

  return (
    <HomePage>
      { !playerVsComputer && !playerVsPlayer ? <ButtonsContainer /> : null }
      { playerVsComputer || playerVsPlayer ? <Game /> : null}
      { isGameOver && <GameResult />}
    </HomePage>
  );
}

export default App;

const HomePage = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100vw;
  height: 100vh;
  background-image: url(${background});
  background-repeat: no-repeat;
  background-size: cover; 
  background-attachment: fixed;
  background-origin: content-box;
  position: relative;
`;