import React, { useEffect } from "react";

import { BoardGame } from "./board.style";

import Square from '../Square/Square.component';

import { useDispatch, useSelector } from "react-redux";
import { DATA_ACTION_TYPES } from "../../store/data/data.types";

const Board = () => {

  const data = useSelector((state) => state.data);
  const { board, movesCounter } = data;

  const dispatch = useDispatch();

  useEffect(() => {
    const { POPULATE_BOARD } = DATA_ACTION_TYPES;
    for (let i = 1; i < 10; i++) {
      const square = { num: i, isEmpty: true, moveNum: null, symbol: "" };
      dispatch({ type: POPULATE_BOARD, payload: square });
    }
  }, [dispatch]);

  useEffect(() => {
    if (movesCounter === 9) {
        dispatch({type: DATA_ACTION_TYPES.GAME_IS_OVER});
    }
}, [movesCounter, dispatch]);

  const showBoardGame = () => {
    if (board.length) {
      return board.map((square) => <Square key={square.num} square={square} /> );
    }
  };

  return <BoardGame>{showBoardGame()}</BoardGame>;
};

export default Board;