import styled from "styled-components";

export const BoardGame = styled.div`
  display: grid;
  grid-template-columns: auto auto auto;
  width: auto;
  height: auto;
  padding: 5%;
`;
