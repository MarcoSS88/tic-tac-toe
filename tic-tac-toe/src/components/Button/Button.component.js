import React from 'react';
import { GlobalButton } from './button.style';

const Button = ({ children, ...otherProps }) => <GlobalButton {...otherProps} > { children } </GlobalButton> ;

export default Button;