import styled from "styled-components";

export const GlobalButton = styled.button`
  width: 17rem;
  height: 3rem;
  background-color: #f8d31f;
  border: none;
  font-family: "Press Start 2P", cursive;
  cursor: pointer;

  &:hover {
    filter: brightness(0.7);
  }
`;
