import React from "react";

import { BtnsContainer, Title } from "./buttonsContainer.style";
import Button from '../Button/Button.component';

import { useDispatch } from 'react-redux';
import { DATA_ACTION_TYPES } from '../../store/data/data.types';

const ButtonsContainer = () => {

  const dispatch = useDispatch();

  const playWithComputer = () => {
    const { PLAYER_VS_COMP } = DATA_ACTION_TYPES;
    dispatch({ type: PLAYER_VS_COMP, payload: true });
  };
  const playWithFriend = () => {
    const { PLAYER_VS_PLAYER } = DATA_ACTION_TYPES;
    dispatch({ type: PLAYER_VS_PLAYER, payload: true });
  };

  return (
    <BtnsContainer>
      <Title>Tris</Title>
      <Button onClick={() => playWithComputer()} >play with computer</Button>
      <Button onClick={() => playWithFriend()} >play with friends</Button>
    </BtnsContainer>
  );
};

export default ButtonsContainer;