import styled from "styled-components";

export const BtnsContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  width: auto;
  height: 15rem;
  margin-top: 3rem;
`;

export const Title = styled.h1`
  color: #f15bb5;
`;
