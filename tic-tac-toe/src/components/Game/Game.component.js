import React from 'react';

import { GamingContainer, Player } from './game.style';

import Board from '../Board/Board.component';

import { useSelector } from 'react-redux';

const PlayerVsComputer = () => {

    const data = useSelector(state => state.data);

    const { playerVsComputer } = data;

    return (
        <GamingContainer>
            <Player>{`Player 1 (x)`}</Player>
            <Board />
            <Player>{playerVsComputer ? "CPU (o)" : "Player 2 (o)"}</Player>
        </GamingContainer>
    );
}

export default PlayerVsComputer;