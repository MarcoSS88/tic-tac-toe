import styled from "styled-components";

export const GamingContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  width: 80%;
  height: 80%;
  background-color: transparent;
`;

export const Player = styled.h2`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 19rem;
  height: 2.5rem;
  background-color: #d0b015;
  margin-bottom: 15rem;
  color: #ffffff;
`;



