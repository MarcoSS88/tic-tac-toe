import React from 'react';

import { Result, ButtonsContainer } from './gameResult.style';

import Button from '../Button/Button.component';

import { useSelector } from 'react-redux';

const GameResult = () => {

    const data = useSelector(state => state.data);
    const { winnerIs } = data;

    const handleH2 = () => {
        switch (winnerIs) {
            case "player 1":
                return "Player 1 wins!"
            case "player 2":
                return "Player 2 wins!"
            case "CPU":
                return "You lose!"
            default:
                return "Game Over!"
        }
    };

    return (
        <Result>
            <h2>{handleH2()}</h2>
            <h3>Do you want to play again ?</h3>
            <ButtonsContainer>
                <Button onClick={() => window.location.reload()}>Yes</Button>
                <Button onClick={() => window.close()}>No</Button>
            </ButtonsContainer>
        </Result>
    );
}

export default GameResult;