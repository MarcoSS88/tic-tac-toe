import styled from "styled-components";

export const Result = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  width: 50%;
  height: 20%;
  border: 1.5px solid #f8d31f;
  position: absolute;
  z-index: 2;
  margin-bottom: 40%;
  background-color: #11213abd;
  color: #ffffff;
  padding: 1% 0 1% 0;
`;

export const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  width: 100%;
  height: 30%;
`;

