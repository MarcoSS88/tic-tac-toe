import React, { Fragment, useEffect } from "react";

import { SquareContainer, Circle, Cross } from "./square.style";

import { useDispatch, useSelector } from "react-redux";

import { DATA_ACTION_TYPES } from "../../store/data/data.types";

const Square = ({ square }) => {

  const data = useSelector((state) => state.data);
  const dispatch = useDispatch();

  const { num, isEmpty, moveNum } = square;
  const { playerVsComputer, movesCounter, board, isGameOver } = data;

  const WINNING_COMBINATION = {
    PLAYER_ONE: {
      ORIZ: {
        ONE:
          board[0].symbol === "x" &&
          board[1].symbol === "x" &&
          board[2].symbol === "x",
        TWO:
          board[3].symbol === "x" &&
          board[4].symbol === "x" &&
          board[5].symbol === "x",
        THREE:
          board[6].symbol === "x" &&
          board[7].symbol === "x" &&
          board[8].symbol === "x"
      },
      VERT: {
        ONE:
          board[0].symbol === "x" &&
          board[3].symbol === "x" &&
          board[6].symbol === "x",
        TWO:
          board[1].symbol === "x" &&
          board[4].symbol === "x" &&
          board[7].symbol === "x",
        THREE:
          board[2].symbol === "x" &&
          board[5].symbol === "x" &&
          board[8].symbol === "x"
      },
      OBLIQ: {
        ONE:
          board[0].symbol === "x" &&
          board[4].symbol === "x" &&
          board[8].symbol === "x",
        TWO:
          board[2].symbol === "x" &&
          board[4].symbol === "x" &&
          board[6].symbol === "x"
      }
    },
    PLAYER_TWO_CPU: {
      ORIZ: {
        ONE:
          board[0].symbol === "o" &&
          board[1].symbol === "o" &&
          board[2].symbol === "o",
        TWO:
          board[3].symbol === "o" &&
          board[4].symbol === "o" &&
          board[5].symbol === "o",
        THREE:
          board[6].symbol === "o" &&
          board[7].symbol === "o" &&
          board[8].symbol === "o"
      },
      VERT: {
        ONE:
          board[0].symbol === "o" &&
          board[3].symbol === "o" &&
          board[6].symbol === "o",
        TWO:
          board[1].symbol === "o" &&
          board[4].symbol === "o" &&
          board[7].symbol === "o",
        THREE:
          board[2].symbol === "o" &&
          board[5].symbol === "o" &&
          board[8].symbol === "o"
      },
      OBLIQ: {
        ONE:
          board[0].symbol === "o" &&
          board[4].symbol === "o" &&
          board[8].symbol === "o",
        TWO:
          board[2].symbol === "o" &&
          board[4].symbol === "o" &&
          board[6].symbol === "o"
      }
    }
  };
  const { PLAYER_ONE, PLAYER_TWO_CPU } = WINNING_COMBINATION;

  useEffect(() => {
    if (movesCounter > 4 && movesCounter <= 9) {
      // player 1 cases
        if (PLAYER_ONE.ORIZ.ONE || PLAYER_ONE.ORIZ.TWO || PLAYER_ONE.ORIZ.THREE) {
            dispatch({type: DATA_ACTION_TYPES.DECLARE_WINNER, payload: "player 1"})
        } else if (PLAYER_ONE.VERT.ONE || PLAYER_ONE.VERT.TWO || PLAYER_ONE.VERT.THREE) {
            dispatch({type: DATA_ACTION_TYPES.DECLARE_WINNER, payload: "player 1"})
        } else if (PLAYER_ONE.OBLIQ.ONE || PLAYER_ONE.OBLIQ.TWO) {
            dispatch({type: DATA_ACTION_TYPES.DECLARE_WINNER, payload: "player 1"})
      // player 2 - CPU cases    
        } else if (PLAYER_TWO_CPU.ORIZ.ONE || PLAYER_TWO_CPU.ORIZ.TWO || PLAYER_TWO_CPU.ORIZ.THREE) {
            dispatch({type: DATA_ACTION_TYPES.DECLARE_WINNER, payload: playerVsComputer ? "CPU" : "player 2"})
        } else if (PLAYER_TWO_CPU.VERT.ONE || PLAYER_TWO_CPU.VERT.TWO || PLAYER_TWO_CPU.VERT.THREE) {
            dispatch({type: DATA_ACTION_TYPES.DECLARE_WINNER, payload: playerVsComputer ? "CPU" : "player 2"})
        } else if (PLAYER_TWO_CPU.OBLIQ.ONE || PLAYER_TWO_CPU.OBLIQ.TWO) {
            dispatch({type: DATA_ACTION_TYPES.DECLARE_WINNER, payload: playerVsComputer ? "CPU" : "player 2"})
        }
    } // eslint-disable-next-line
  }, [movesCounter, playerVsComputer, dispatch]);

  useEffect(() => {
    if (playerVsComputer && movesCounter % 2 !== 0 && !isGameOver) {
      const interval = setInterval(() => {
        dispatch({ type: DATA_ACTION_TYPES.CPU_MOVE })
      }, 2000);

      return () => clearInterval(interval);
    }
  }, [playerVsComputer, movesCounter, isGameOver, dispatch]);

  const playerMove = (i) => {
    const { PLAYER_MOVE } = DATA_ACTION_TYPES;
    dispatch({ type: PLAYER_MOVE, payload: i });
  };

  return (
    <Fragment>
      {isEmpty ? (
        <SquareContainer onClick={!isGameOver ? () => playerMove(num) : null} />
      ) : (
        <SquareContainer>
          {moveNum % 2 === 0 ? (
            <Circle className="material-symbols-outlined">trip_origin</Circle>
          ) : (
            <Cross className="material-symbols-outlined">close</Cross>
          )}
        </SquareContainer>
      )}
    </Fragment>
  );
};

export default Square;