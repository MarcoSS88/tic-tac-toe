import styled from "styled-components";

export const SquareContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 7rem;
  height: 7rem;
  border: 2.5px solid #EAD4BC;
  background-color: #11213a;
  cursor: pointer;
`;

export const Circle = styled.span`
  zoom: 2;
  color: #D5A727;
`;

export const Cross = styled.span`
  zoom: 2.85;
  color: #05B9A6;
`;

