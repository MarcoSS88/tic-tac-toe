import { createAction } from "../../utils/reducer/reducer.utils";
import { DATA_ACTION_TYPES } from "./data.types";

export const playWithComputer = (boolean) => createAction(DATA_ACTION_TYPES.PLAYER_VS_COMP, boolean);

export const playWithFriend = (boolean) => createAction(DATA_ACTION_TYPES.PLAYER_VS_PLAYER, boolean);

export const populateBoard = (data) => createAction(DATA_ACTION_TYPES.POPULATE_BOARD, data);

export const playerMove = (data) => createAction(DATA_ACTION_TYPES.PLAYER_MOVE, data);

export const cpuMove = (data) => createAction(DATA_ACTION_TYPES.CPU_MOVE, data);

export const declareWinner = (data) => createAction(DATA_ACTION_TYPES.DECLARE_WINNER, data);

export const gameIsOver = (boolean) => createAction(DATA_ACTION_TYPES.GAME_IS_OVER, boolean);