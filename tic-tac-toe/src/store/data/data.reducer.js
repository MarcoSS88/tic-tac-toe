import { DATA_ACTION_TYPES } from './data.types';

const STATE = {
    playerVsComputer: false,
    playerVsPlayer: false,
    board: [],
    movesCounter: 0, 
    isGameOver: false, 
    winnerIs: null
}

const DataReducer = (state = STATE, action) => {

    const { type, payload } = action;

    let manipolationArray = [], emptySquares = [], possibleMoves = [];
    let randomMove = null, includedMove = null;

    switch (type) {
        
        case DATA_ACTION_TYPES.PLAYER_VS_COMP:
            return { ...state, playerVsComputer: payload };
        
        case DATA_ACTION_TYPES.PLAYER_VS_PLAYER:
            return { ...state, playerVsPlayer: payload };

        case DATA_ACTION_TYPES.POPULATE_BOARD:
            return { ...state, board: [...state.board, payload] };

        case DATA_ACTION_TYPES.PLAYER_MOVE:
            manipolationArray = state.board.map(square => {
                if (square.num === payload && square.isEmpty) {
                    return { 
                        ...square, isEmpty: false, 
                        moveNum: state.movesCounter + 1, 
                        symbol: (state.movesCounter + 1) % 2 !== 0 ? "x" : "o"
                    };
                } else return square;
            })
            return { ...state, movesCounter: state.movesCounter + 1, board: manipolationArray };

        case DATA_ACTION_TYPES.DECLARE_WINNER:
            return { ...state, isGameOver: true, winnerIs: payload };

        case DATA_ACTION_TYPES.GAME_IS_OVER:
            return { ...state, isGameOver: true };

        case DATA_ACTION_TYPES.CPU_MOVE:
            emptySquares = state.board.filter(square => square.isEmpty);
            possibleMoves = emptySquares.map(square => square.num);

            do {
                randomMove = Math.floor(Math.random() * 9) + 1;
                includedMove = possibleMoves.includes(randomMove);
            } while (!includedMove);

            if (includedMove) {
                manipolationArray = state.board.map((square, index) => {
                if (square.isEmpty && square.num === randomMove && randomMove === (index + 1)) {
                    return { ...square, isEmpty: false, moveNum: state.movesCounter + 1, symbol: "o" };
                } else return square;
            })
            } 
            return { ...state, movesCounter: state.movesCounter + 1, board: manipolationArray };

        default:
            return state;
    }

}

export default DataReducer;